# Check\_MK Raw Edition (CRE) Docker

![](https://gitlab.com/morph027/check_mk-raw-edition-docker/raw/debian/check_mk-docker-logo.png)

[Check_MK](http://mathias-kettner.com/check_mk.html) is an amazing monitoring project. And Docker is perfect for shipping apps.

This project aims on delivering a Docker image which allows you to qickly setup a _Check_MK Raw Edition_ instance.

## Usage

### Credentials

_Check_MK_ will create a random password for user _cmkadmin_ on first start, check the container logs with `docker logs cre-demo`.

### Default demo site

Just start an instance of the image and you'll get a website at http://localhost/demo/check_mk/ :

```bash
$ docker run --name cre-demo \
-d -p 80:5000 \
-v /etc/localtime:/etc/localtime \
registry.gitlab.com/morph027/check_mk-raw-edition-docker:1.4.0-debian-1
```

The ```-v /etc/localtime:/etc/localtime``` sets your time inside the container and therefore the service.

### Custom site with persistent data

If you want to store your data for longtime usage, you'll need to setup some volumes.

Prepare the volume for **omd**:

```bash
$ sudo mkdir -p /opt/omd/sites
```

Run the service:

```bash
$ docker run --name cre-morph027 \
-e "SITE_NAME=morph027" \
-d -p 80:5000 \
-v /omd/sites:/omd/sites \
-v /etc/localtime:/etc/localtime \
registry.gitlab.com/morph027/check_mk-raw-edition-docker:1.4.0-debian-1
```

### Enable SSH

The image ships with dropbear, you can enable by passing ```-e 'SSH=true'``` and map a port to ```22```. The password for ```root``` is ```root```.

```bash
$ docker run --name cre-demo \
-d -p 80:5000 \
-p 2222:22 \
-e 'SSH=true' \
-v /etc/localtime:/etc/localtime \
registry.gitlab.com/morph027/check_mk-raw-edition-docker:1.4.0-debian-1
```

### Enable MKEVENTD

If you want to use [Event Console (MKEVENTD)](http://mathias-kettner.com/checkmk_mkeventd.html), you'll need to run your instance in ```privileged``` mode, because it depends on ```tpmfs```.

Run the service (it will detect ```privileged``` mode and enable ```MKEVENTD```):

```bash
$ docker run --name cre-morph027 \
-e "SITE_NAME=morph027" \
-d -p 80:5000 \
-v /opt/omd/sites:/omd/sites \
-v /etc/localtime:/etc/localtime \
--privileged \
registry.gitlab.com/morph027/check_mk-raw-edition-docker:1.4.0-debian-1
```

### Install Addons

You can extend _Check\_MK_ with addons. To do so in an instance of this project, just provide another volume with ```*.mkp``` files in it. The volume needs to be mounted at ```/opt/$SITE_NAME-addons```.

#### Example

* Get an addon:

```bash
$ mkdir -p /omd/addons/
$ curl -L https://github.com/HeinleinSupport/check_mk/blob/master/ceph/ceph-2.0.mkp?raw=true -o /omd/addons/ceph-2.0.mkp
```

* Run the service:

```bash
$ export SITE_NAME=morph027
$ docker run --name cre-$SITE_NAME \
-e "SITE_NAME=$SITE_NAME" \
-d -p 80:5000 \
-v /omd/sites:/omd/sites \
-v /etc/localtime:/etc/localtime \
-v /omd/addons/:/opt/$SITE_NAME-addons \
registry.gitlab.com/morph027/check_mk-raw-edition-docker:1.4.0-debian-1
```

### Mail notifications

If you want, you can enable a simple postfix mail relay by passing the variable ```RELAY_HOST```.

```bash
$ export SITE_NAME=morph027
$ docker run --name cre-$SITE_NAME \
-e "SITE_NAME=$SITE_NAME" \
-e "RELAY_HOST=my.smtp.server" \
-d -p 80:5000 \
-v /omd/sites:/omd/sites \
-v /etc/localtime:/etc/localtime \
registry.gitlab.com/morph027/check_mk-raw-edition-docker:1.4.0-debian-1
```

#### Custom Settings

You can add custom postfix settings by mounting a volume to ```/opt/${OMD_SITE_NAME}-postfix```, which must contain a file named ```config``` with postfix settings. The content will be added to ```/etc/postfix/main.cf```.

#### Example: Authentication

* Create a config file with some content:

```
smtp_sasl_auth_enable = yes
smtp_sasl_password_maps = hash:/etc/postfix/auth # Must be named auth
smtp_tls_security_level = may
smtp_use_tls=yes
smtp_sasl_security_options = noanonymous
relayhost = my.mail.server # using this, we can skip setting $RELAY_HOST
```

* Create an authorization file:

```
my.mail.server	my-user@my.mail.server:my-secret-password
```

* Run the service:

```bash
$ export SITE_NAME=morph027
$ docker run --name cre-$SITE_NAME \
-e "SITE_NAME=$SITE_NAME" \
-d -p 80:5000 \
-v /omd/sites:/omd/sites \
-v /etc/localtime:/etc/localtime \
-v /omd/postfix:/opt/${OMD_SITE_NAME}-postfix \
registry.gitlab.com/morph027/check_mk-raw-edition-docker:1.4.0-debian-1
```

### OMD settings

To change settings, just attach or [ssh](#enable-ssh) to your running instance and execute ```omd``` commands:

```bash
$ docker exec -it cre-demo /bin/bash
$ omd stop demo
$ omd config demo
# ... do things
$ omd start demo
```