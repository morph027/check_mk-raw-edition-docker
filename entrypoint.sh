#!/bin/bash

OMD_SITE_NAME=${SITE_NAME:-demo}

_dropbear_init() {
sed -i 's,NO_START=1,NO_START=0,' /etc/default/dropbear
}

_postfix_init() {
local RELAY_HOST=${RELAY_HOST:-smtp}
debconf-set-selections <<EOF
postfix postfixpostfix/mailname string $HOSTNAME
postfix postfix/destinations string $HOSTNAME, localhost.localdomain, localhost
postfix postfix/main_mailer_type select Satellite system
postfix postfix/relayhost string $RELAY_HOST
EOF
DEBIAN_PRIORITY=critical dpkg-reconfigure postfix
postconf -e "relayhost = $RELAY_HOST"
if [ -f /opt/${OMD_SITE_NAME}-postfix/config ]; then
  while read LINE
  do
    postconf -e "$LINE"
  done < /opt/${OMD_SITE_NAME}-postfix/config
fi
if [ -f /opt/${OMD_SITE_NAME}-postfix/auth ]; then
   cp /opt/${OMD_SITE_NAME}-postfix/auth /etc/postfix/smtp_auth
   postmap /etc/postfix/smtp_auth
fi
}

if [ ! -z $RELAY_HOST ] || [ -d /opt/${OMD_SITE_NAME}-postfix ] ; then
  echo "setting up postfix ..."
  _postfix_init
  service inetutils-syslogd start
  service postfix start
else
  mkdir -p /var/log/mail
  touch /var/log/mail/mail.log
fi

if [ ! -z $SSH ]; then
  _dropbear_init
  service dropbear start
fi

if [ ! -d /opt/omd/sites/"$OMD_SITE_NAME" ]; then
  echo "site $OMD_SITE_NAME does not exist, creating ..."
  omd create "$OMD_SITE_NAME" 2>&1
  omd config "$OMD_SITE_NAME" set APACHE_TCP_ADDR 0.0.0.0
  omd config "$OMD_SITE_NAME" set MKEVENTD off
else
  getent passwd "$OMD_SITE_NAME" || (
    echo "site $OMD_SITE_NAME already exists, adjusting system"
    useradd -M -d /omd/sites/"$OMD_SITE_NAME" -s /bin/bash -c "OMD site $OMD_SITE_NAME" -G omd "$OMD_SITE_NAME"
  )
  echo "tmpfs  /opt/omd/sites/$OMD_SITE_NAME/tmp tmpfs noauto,user,mode=755,uid=$OMD_SITE_NAME,gid=$OMD_SITE_NAME 0 0" >> /etc/fstab
  chown -R "$OMD_SITE_NAME": /omd/sites/"$OMD_SITE_NAME"
  omd enable $OMD_SITE_NAME 2>&1
fi

ip link add dummy0 type dummy >/dev/null 2>&1
if [ $? -eq 0 ]; then
  echo "running in privileged mode, enabling TMPFS ..."
  omd config "$OMD_SITE_NAME" set TMPFS on
  omd config "$OMD_SITE_NAME" set MKEVENTD on
else
  echo "not running in privileged mode, disabling TMPFS ..."
  omd config "$OMD_SITE_NAME" set TMPFS off
  omd config "$OMD_SITE_NAME" set MKEVENTD off
  touch /opt/omd/sites/$OMD_SITE_NAME/var/log/mkeventd.log # prevent tail from printing error about non-existing file
fi

echo "starting site $OMD_SITE_NAME ..."
su - "$OMD_SITE_NAME" -c "omd start" 2>&1

if [ -d /opt/${OMD_SITE_NAME}-addons ]; then
  echo "installing addons ..."
  find /opt/${OMD_SITE_NAME}-addons -type f -name "./*mkp" -exec su - "$OMD_SITE_NAME" -c "check_mk -v -P install {}" \;
fi

cd /opt/omd/sites/"$OMD_SITE_NAME"/var/log && tail  -f nagios.log rrdcached.log mkeventd.log livestatus.log apache/error_log /var/log/mail.log
